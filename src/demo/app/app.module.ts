import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ngxMapboxModule } from 'ngx-mapbox';

import { AppComponent }  from './app.component';

@NgModule({
  imports:      [
    BrowserModule,
    ngxMapboxModule.forRoot('YOUR_KEY')],
  declarations: [ AppComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
