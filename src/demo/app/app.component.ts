import { Component } from '@angular/core';
import { ngxMapboxService } from 'ngx-mapbox';

@Component({
  selector: 'demo-app',
  templateUrl: './app.component.html'
})
export class AppComponent {
  version: string;
  constructor(libService: ngxMapboxService) {
    this.version = libService.mapbox.version
  }
}
