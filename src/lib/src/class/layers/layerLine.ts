import { Layer } from './layer';
import { Type } from './enums';
export class LayerLine extends Layer{
  constructor(line : {
    id:string,
    metadata?:any,
    ref?:string,
    source?:string,
    sourceLayer?:string,
    minzoom?:number,
    maxzoom?:number,
    filter?:any,
    layout?:any,
    paint?:any
  }){
    line['type'] = 'line';
    super(line);
  }
}
