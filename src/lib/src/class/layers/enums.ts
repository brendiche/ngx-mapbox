type  Type  =
  'fill'|
  'line'|
  'symbol'|
  'circle'|
  'fill-extrusion'|
  'raster'|
  'background'

export {Type};
