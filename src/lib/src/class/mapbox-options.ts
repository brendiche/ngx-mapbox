export class MapboxOptions {
	private _container:any;
	private _minZoom:number;
	private _maxZoom:number;
	private _style:any;
	private _hash:any;
	private _interactive:any;
	private _bearingSnap:any;
	private _pitchWithRotate:any;
	private _classes:any;
	private _attributionControl:any;
	private _logoPosition:any;
	private _failIfMajorPerformanceCaveat:any;
	private _preserveDrawingBuffer:any;
	private _refreshExpiredTiles:any;
	private _maxBounds:any;
	private _scrollZoom:any;
	private _boxZoom:any;
	private _dragRotate:any;
	private _dragPan:any;
	private _keyboard:any;
	private _doubleClickZoom:any;
	private _touchZoomRotate:any;
	private _trackResize:any;
	private _center:any;
	private _zoom:any;
	private _pitch:any;
	private _renderWorldCopies:any;
	constructor(){

	}
}
