import { NgModule, ModuleWithProviders } from '@angular/core';

import { ngxMapboxComponent } from './component/ngxMapbox.component';
import { ngxMapboxService } from './service/ngxMapbox.service';


@NgModule({
  declarations: [ngxMapboxComponent],
  providers: [ngxMapboxService],
  exports: [ngxMapboxComponent]
})
export class ngxMapboxModule {
  static forRoot(config: String): ModuleWithProviders {
    return {
      ngModule: ngxMapboxModule,
      providers: [
        { provide: 'MAPBOX_KEY', useValue: config },
        ngxMapboxService
      ]
    };
  }
}
