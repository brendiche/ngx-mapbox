import { Injectable, Inject, EventEmitter, NgZone, Optional } from '@angular/core';
import { Control } from '../interface/control';
import { Layer, Source } from '../class/class';

import * as mapboxgl from 'mapbox-gl';

@Injectable()
export class ngxMapboxService {
  private accessToken: String;
  public mapbox : any;
  constructor(private zone: NgZone,
    @Optional() @Inject('MAPBOX_KEY') private readonly MAPBOX_API_KEY: string) {
    this.zone.runOutsideAngular(()=>{
      mapboxgl.accessToken = MAPBOX_API_KEY;
    });
     this.mapbox = mapboxgl;
  }

  public createMap(options: Object): Object {
    return new mapboxgl.Map(options);
  }

  /**
   * Handle genenral control of the map
   */
  public addControl(map: any, control: Control, position?: string): void {
    if (position) {
      map.addControl(control, position);
    } else {
      map.addControl(control);
    }
  }

  public removeControl(map: mapboxgl.Map, control: Control): void {
    map.removeControl(control);
  }

  /**
   * Handle navigation control of the map
   */
  public addNavigationControl(map: any, position: string): Control {
    let control: Control = new mapboxgl.NavigationControl();
    map.addControl(control, position);
    return control;
  }

  /**
   * Handle scale control of the map
   */
  public addScaleControl(map: mapboxgl.Map, position: string, options?: { maxWidth?: number, unit?: string }): Control {
    let control: Control = new mapboxgl.ScaleControl(options);
    map.addControl(control, position);
    return control;
  }


  /**
   * Handle layers
   */
  public addLayer(map: mapboxgl.Map, layer: Layer) {
    if (map.getSource(layer.source)) {
      map.addLayer(layer.toJSON());
    }
  }

  public removeLayer(map: mapboxgl.Map, layer: Layer) {
    if (map.getLayer(layer.id)) {
      map.removeLayer(layer.id);
    }
  }

  /**
   * Handle sources
   */
  public addSource(map: mapboxgl.Map, source: Source): void {
    map.addSource(source.id, source.toJson());
  }
  public removeSource(map: mapboxgl.Map, source: Source): void {
    if (map.isSourceLoaded(source.id)) {
      map.removeSource(source.id);
    }
  }

  public addListener(map, event, listener) {
    map.on(event, listener);
  }

  public addListenerOnce(map, event, listener) {
    map.once(event, listener);
  }

  public removeListener(map, event, listener) {
    map.off(event, listener);
  }
}
