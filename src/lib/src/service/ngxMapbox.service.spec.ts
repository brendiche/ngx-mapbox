import { TestBed, inject } from '@angular/core/testing';

import { ngxMapboxService } from './ngxMapbox.service';

describe('ngxMapboxService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ngxMapboxService]
    });
  });

  it('should create service', inject([ngxMapboxService], (service: ngxMapboxService) => {
    expect(service).toBeTruthy();
  }));

  it('should return 42 from getMeaning', inject([ngxMapboxService], (service: ngxMapboxService) => {
    expect(service.getMeaning()).toBe(42);
  }));
});
