export { ngxMapboxComponent } from './src/component/ngxMapbox.component';
export { ngxMapboxService } from './src/service/ngxMapbox.service';
export { ngxMapboxModule } from './src/module';
export { Layer, LayerLine, MapboxOptions, Marker, Type, Source, sourceType } from './src/class/class'
export { mapEvent } from './src/component/enums';
export { Control } from './src/interface/control';
